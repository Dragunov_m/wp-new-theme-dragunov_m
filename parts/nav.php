<!--
<nav id="menu">
    <ul class="links">
        <li><a href="#"></a></li>
        <li><a href="#"></a></li>
        <li><a href="#"></a></li>
    </ul>
</nav>
-->

<?php
    wp_nav_menu ( array(
        'theme_location' => 'main_menu',
        'container' => 'nav',
        'container_id' => 'menu',
        'menu_class' => 'links',
        'depth' => 0
    ) );
?>

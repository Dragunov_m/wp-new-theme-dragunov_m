<!DOCTYPE HTML>

<html>
	<head>
		<title>Transitive by TEMPLATED</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<?php wp_head() ?>
	</head>
	<body <?php body_class() ?>>

		<!-- Header -->
			<header id="header" class="alt">
				<div class="logo"><a href="index.html">Transitive <span>by TEMPLATED</span></a></div>
				<a href="#menu" class="toggle"><span>Menu</span></a>
			</header>

		<!-- Nav -->
        <?php get_template_part('parts/nav') ?>
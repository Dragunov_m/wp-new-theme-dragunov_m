<?php

//add_action( "admin_enqueue_scripts", "style");
add_action( "wp_enqueue_scripts", "style" /* функция */); /* хук */

  function style(){ /* функция с именем style */

    wp_enqueue_style( 'test-style', get_stylesheet_directory_uri() . ' /assets/css/main.css', false, time() ); // added directory stylesheet

    wp_deregister_script( 'jquery' ); // отмена регистрации стоковой директории wp jquery

    wp_register_script( 'jquery', includes_url( 'js/jquery/jquery.js' ) /* относительная директория встроенного jquery в папке includes */, false /* зависимость */, null /* версия */, true /* подключение в footer */ ); // регистрация новой относительной директории

    wp_enqueue_script( 'jquery' ); // инициация jquery

    wp_enqueue_script( 'test-scrolly', get_stylesheet_directory_uri() . ' /assets/js/jquery.scrolly.min.js', 'jquery', null, true ); // добавление директории scrolly

    wp_enqueue_script( 'test-scrollex', get_stylesheet_directory_uri() . ' /assets/js/jquery.scrollex.min.js', array('jquery','test-scrolly') /* массив зависимостей */, null, true ); // добавление директории scrollex

    wp_enqueue_script( 'test-skel', get_stylesheet_directory_uri() . ' /assets/js/skel.min.js', 'jquery', null, true ); // добавление директории skel

    wp_enqueue_script( 'test-util', get_stylesheet_directory_uri() . ' /assets/js/util.js', 'jquery', null, true ); // добавление директории util

    wp_enqueue_script( 'test-main', get_stylesheet_directory_uri() . ' /assets/js/main.js', 'jquery', time(), true ); // добавление директории main

  }

  register_nav_menus( array(
  	'main_menu' => 'Главное меню'
  ) );

?>